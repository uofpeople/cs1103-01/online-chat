# Chat Application

## Overview

This is a simple Java-based chat application using socket programming. the applciation includes a server that manages connections from multiple clients, and clients that can send and receive messages to and from the server.

## Setup Requirements
- Java Development Kit (JDK) installed

## How to run the server
1. Compile the `ChatServer.java` file which is in the `src` directory:
    ```sh
    javac ChatServer.java
    ```
2. Run the `ChatServer`:
    ```sh
    java ChatServer
    ```

## How to run the client
1. Compile the `ChatClient.java` which is the the `src` directory:
    ```sh
    javac ChatClient.java
    ```
2. Run the `ChatClient`:
    ```sh
    java ChatClient
    ```
## How it works
### ChatServer
- Uses `ServerSocket` to listen for incoming client connections
- Assigns a unique user ID  to each connected client.
- Manages a list of connected Clients.
- Broadcasts messages to all connected clients.

### ChatClient
- Connects to the server using `Socket`.
- Sends messages to the server which are then broadcasted to all connected clients.
- Receives messages from the server and displays them.

## UI Display
- Simple text-based interface using `Scanner` class for getting the user input.
- Messages from other users are displayed in the console.

## Example
1. Start the server:
    ```
    Chat Server started on port 12345
    ```
2. Start a client.
    ```
    User 0 has joined the chat.
3. Start another console window to start a client again.
    ```
    User 0 has joined the chat.
    User 1 has joined the chat.
    ```
4. Send a message:
    ```
    User 0 : Hello World!
    ```
5. The message will be broadcasted to all connected clients:
    ```
    User 0 : Hello World!
    ```
6. When the Client window is closed the message will be broadcasted to all connected clients.
    ```
    User 0 has left the chat.
    ```