import java.io.*;
import java.net.*;
import java.util.*;

public class ChatServer {
    private static int uniqueId = 0;
    private final List<ClientThread> clients = Collections.synchronizedList(new ArrayList<>());
    public static void main(String [] args){
        ChatServer server = new ChatServer();
        server.start(12345);
    }

    public void start(int port) {
        // Use ServerSocket to listen for incoming client connections.
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            System.out.println("Chat Server started on port " + port);

            while (true) {
                Socket socket = serverSocket.accept();
                ClientThread client = new ClientThread(socket, uniqueId++);
                clients.add(client);
                client.start();
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }
    
    // Send messages to the server.
    private synchronized void broadcast(String message){
        for (ClientThread client : clients) {
            client.sendMessage(message);
        }
    }

    class ClientThread extends Thread {
        Socket socket;
        ObjectOutputStream out;
        int id;

        ClientThread(Socket socket, int id) {
            this.socket = socket;
            this.id = id;
        }

        public void run() {
            try {
                out = new ObjectOutputStream(socket.getOutputStream());
                ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
                // broadcast to all connected clients.
                broadcast("User " + id + " has joined the chat.");

                //Receive messages from the server and displays them
                String message;
                while((message = (String) in.readObject()) != null) {
                    broadcast("User " + id + " : " + message);
                }
            }catch (IOException | ClassNotFoundException e){
                e.printStackTrace();
            }finally {
                try {
                    socket.close();
                }catch (IOException e) {
                    e.printStackTrace();
                }
                clients.remove(this);
                broadcast("User " + id + " has left the chat.");
            }
        }

        private void sendMessage(String message) {
            try {
                out.writeObject(message);
            }catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}