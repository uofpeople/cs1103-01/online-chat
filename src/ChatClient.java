import java.io.*;
import java.net.*;
import java.util.Scanner;


public class ChatClient {
    private ObjectOutputStream out;
    private ObjectInputStream in;
    private Socket socket;

    public static void main(String[] args){
        //Connect to the server's port
        ChatClient client = new ChatClient("localhost", 12345);
        client.start();
    }

    public ChatClient(String serverAddress, int serverPort) {
        try {
            socket = new Socket(serverAddress, serverPort);
            out = new ObjectOutputStream(socket.getOutputStream());
            in = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start() {
        new ReadThread().start();

        try (Scanner scanner = new Scanner(System.in)){
            while(true) {
                String message = scanner.nextLine();
                sendMessage(message);
            }
        } 
    }
    // send messages
    private void sendMessage(String message) {
        try {
            out.writeObject(message);
        } catch(IOException e){
            e.printStackTrace();
        }
    }

    class ReadThread extends Thread {
        public void run() {
            try {
                while(true) {
                    String message = (String) in.readObject();
                    System.out.println(message);
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}